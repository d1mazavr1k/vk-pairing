import os
import sys
sys.path.insert(0, os.path.abspath('../code'))

project = 'vk-pairing'
copyright = '2020, Dmitrii Smirnov, Dmitrii Tsvetkov'
author = 'Dmitrii Smirnov, Dmitrii Tsvetkov'
release = '1.0'
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx.ext.todo',
]
templates_path = ['_templates']
master_doc = 'index'
language = 'en'
hmtl_theme = "alabaster"
html_static_path = ['_static']