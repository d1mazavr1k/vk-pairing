import pandas as pd
import math
import requests
import time


class VKDataGetter:

    def __init__(self, vk_token):
        self.vk_token = vk_token

    def getUserIDs(self, vk_group_id, count=1000):
        """Returns pandas DataFrame with information about
        group subscribers.

        :param vk_group_id: id of VK group
        :type vk_group_id: int
        :param count: count of subscribers to get
        :type count: int

        :return: dataframe with info about subscribers
        :rtype: pandas.DataFrame
        """

        vk_api_url = "https://api.vk.com/method/groups.getMembers"
        query_param = {
            'group_id': vk_group_id,
            'offset': 0,
            'fields': 'name,photo_max_orig',
            'count': count if count < 1000 else 1000,
            'access_token': self.vk_token,
            'v': 5.126
        }

        response = requests.get(vk_api_url, params=query_param)
        response.raise_for_status()
        response_data = response.json()['response']
        users_data = response_data['items']

        if count > 1000:
            getUsersCount = response_data['count']
            print(f"Total number of subscribers = {getUsersCount}")
            numOfRequests = math.ceil(min(getUsersCount, count) / 1000)

            if numOfRequests > 1:
                for i in range(1, numOfRequests):
                    query_param['offset'] = i * 1000
                    cur_response = requests.get(vk_api_url, params=query_param)
                    cur_response.raise_for_status()
                    cur_data = cur_response.json()['response']['items']
                    users_data += cur_data
                    time.sleep(1)

        df = pd.DataFrame(data=users_data)
        df['deactivated'] = df['deactivated'].notnull()
        values = {'can_access_closed': False, 'is_closed': True}
        df = df.fillna(value=values)

        return df

    def getUsersGroups(self, UserID):
        """Returns list of group IDs for groups
        the user with UserID is subscribed to.

        :param UserID: id of VK user
        :type UserID: int

        :return: list of group IDs
        :rtype: list
        """

        vk_api_url = "https://api.vk.com/method/groups.get"
        query_param = {
            'user_id': UserID,
            'offset': 0,
            'access_token': self.vk_token,
            'v': 5.126
        }

        response = requests.get(vk_api_url, params=query_param)
        response.raise_for_status()
        response_data = response.json()['response']
        getGroupsCount = response_data['count']
        numOfRequests = math.ceil(getGroupsCount / 1000)
        user_groups = response_data['items']

        if(numOfRequests > 1):
            for i in range(1, numOfRequests):
                query_param['offset'] = i * 1000
                cur_response = requests.get(vk_api_url, params=query_param)
                cur_response.raise_for_status()
                cur_data = cur_response.json()['response']['items']
                user_groups += cur_data
                time.sleep(1)

        return user_groups
